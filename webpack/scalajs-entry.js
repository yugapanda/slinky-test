if (process.env.NODE_ENV === "production") {
    const opt = require("./slinkytest-opt.js");
    opt.main();
    module.exports = opt;
} else {
    var exports = window;
    exports.require = require("./slinkytest-fastopt-entrypoint.js").require;
    window.global = window;

    const fastOpt = require("./slinkytest-fastopt.js");
    fastOpt.main()
    module.exports = fastOpt;

    if (module.hot) {
        module.hot.accept();
    }
}
