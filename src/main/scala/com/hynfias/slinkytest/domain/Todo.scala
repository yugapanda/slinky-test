package com.hynfias.slinkytest.domain

case class Todo(name: String, done: Boolean)
