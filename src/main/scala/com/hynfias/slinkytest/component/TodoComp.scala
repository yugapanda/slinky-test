package com.hynfias.slinkytest.component

import com.hynfias.slinkytest.domain.Todo
import org.scalajs.dom.{Event, html}
import slinky.core.{FunctionalComponent, SyntheticEvent}
import slinky.core.annotations.react
import slinky.core.facade.Hooks.useState
import slinky.web.html.{button, div, input, onChange, onClick, value}

import scala.util.chaining.scalaUtilChainingOps

@react object TodoComp {
  case class Props()
  val component = FunctionalComponent[Props] { props =>
    val (todoState, setTodoState) = useState(Seq[Todo]())

    def addTodo(): Unit = setTodoState(Todo("", done = false) +: todoState)

    def editTodoName(index: Int, name: String): Unit = {
      todoState.updated(index, Todo(name, todoState(index).done)).pipe(setTodoState)
    }

    div(
      todoState.zipWithIndex.map(x => {
        input(
          onChange := ((e) => editTodoName(x._2, e.currentTarget.value)),
          value := todoState(x._2).name
        )
      }),
      button("Todoを追加", onClick := (() => addTodo()))
    )
  }
}
